#!/bin/bash

if test -z "$1"
then
 echo "Lets, GO!"
 ssh robbiestoffel@75.178.116.109
else
  if [[ $1 == local ]]
  then
   echo "Lets, GO!"
   ssh pi@192.168.86.44
  else
    if [[ $1 == pi ]]
    then
     echo "Lets, GO!"
     ssh -p 25564 pi@75.178.116.109
    fi
  fi
fi
